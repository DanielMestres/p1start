package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.JsonError;
import edu.uprm.cse.datastructures.cardealer.util.NotFoundException;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	private final SortedList<Car> list = CarList.getInstance();

	/*
	 * Returns an array containing the cars in the list
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] result = new Car[list.size()];
		int count = 0;
		
		for(Car e : list) {
			result[count++] = e;
		}
		return result;
	}
	
	/*
	 * Returns car with given id, returns an error otherwise
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		for(Car e : list) {
			if (e.getCarId() == id) {
				return e;
			}
		}
		throw new WebApplicationException(Response.Status.NOT_FOUND);
	}

	/*
	 * Adds car to list if its not present, returns an error if its present
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		for(Car e : list) {
			if (e.getCarId() == car.getCarId()) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
		}
		
		list.add(car);
		return Response.status(Response.Status.CREATED).build();
	}
	
	/*
	 * Deletes car with given id if its present, returns an error otherwise
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		for(Car e : list) {
			if (e.getCarId() == id) {
				list.remove(e);
				return Response.status(Response.Status.OK).build(); 
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	/*
	 * Replaces car that matches given id with the given car, returns an error otherwise
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(@PathParam("id") long id, Car car) {
		for(Car e : list) {
			if (e.getCarId() == id) {
				list.remove(e);
				list.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}