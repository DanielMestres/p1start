package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {
	/* 
	 * Compares two cars in the order: Brand -> Model -> Option.
	 * Returns a value < 0 or > 0 if brand, model or option is different.
	 * Returns 0 if brand, model and option are equal.
	 */
	@Override
	public int compare(Car c1, Car c2) {
		if(c1.getCarBrand().compareTo(c2.getCarBrand()) != 0) {
			return c1.getCarBrand().compareTo(c2.getCarBrand());
		} else
			if(c1.getCarModel().compareTo(c2.getCarModel()) != 0) {
				return c1.getCarModel().compareTo(c2.getCarModel());
			} else
				if(c1.getCarModelOption().compareTo(c2.getCarModelOption()) != 0) {
					return c1.getCarModelOption().compareTo(c2.getCarModelOption());
				} else
					return 0;
	}
}